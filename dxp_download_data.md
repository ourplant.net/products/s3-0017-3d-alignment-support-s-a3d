Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0017-3d-alignment-support-s-a3d).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         | [de](https://gitlab.com/ourplant.net/products/s3-0017-3d-alignment-support-s-a3d/-/raw/main/01_operating_manual/S3-0017_B2_Betriebsanleitung.pdf)                 |
| assembly drawing         | [de](https://gitlab.com/ourplant.net/products/s3-0017-3d-alignment-support-s-a3d/-/raw/main/02_assembly_drawing/s3-0017-a_3d_alignment_support.PDF)                  |
| circuit diagram          | [de](https://gitlab.com/ourplant.net/products/s3-0017-3d-alignment-support-s-a3d/-/raw/main/03_circuit_diagram/S3-0017_3D-Substrataufnahme_S-A3D.pdf)                  |
| maintenance instructions |                  |
| spare parts              | [de](https://gitlab.com/ourplant.net/products/s3-0017-3d-alignment-support-s-a3d/-/raw/main/05_spare_parts/S3-0017_B_EVL.pdf) ,    [en](https://gitlab.com/ourplant.net/products/s3-0017-3d-alignment-support-s-a3d/-/raw/main/05_spare_parts/S3-0017_B_EVL_engl.pdf)             |

